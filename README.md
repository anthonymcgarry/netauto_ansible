Holds ansible files used in automating the devices in the netauto_labconfig repo 

https://gitlab.com/anthonymcgarry/netauto_labconfig

__

https://gitlab.com/anthonymcgarry/netauto_ansible/blob/master/playbooks/get/show_interfaces.yaml

Show interfaces on IOS/IOS-XR/JUNOS/NX-OS and parse output using 
ansible-network.network-engine module to select relevant info and format in JSON

__
